import { combineReducers} from 'redux';
import * as auth from './auth';
import * as signup from './signup';
import { reducer as formReducer } from 'redux-form';
import { i18nReducer } from 'react-redux-i18n';
import firebaseReducers from '../services/firebase/redux';


export default combineReducers ({
    signup: signup.reducer,
    auth: auth.reducer,
    form: formReducer,
    i18n: i18nReducer,
    ...firebaseReducers
});