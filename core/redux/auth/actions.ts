//import firebase from 'firebase';
//import { Actions } from 'react-native-router-flux';
import AuthService from '../../services/firebase/auth/authService';
import { FirebaseAuthError } from '../../services/firebase/auth/firebaseAuths/firebaseAuthInterface';

export enum ActionTypes {
    EMAIL_CHANGED = '[LOGIN] EMAIL_CHANGED',
    PASSWORD_CHANGED = '[LOGIN] PASSWORD_CHANGED',
    LOGIN_USER_SUCCESS = '[LOGIN] LOGIN_USER_SUCCESS',
    LOGIN_USER_ERROR = '[LOGIN] LOGIN_USER_ERROR',
    DOING_LOGIN = '[LOGIN] DOING_LOGIN'
}

export interface EmailChangedAction { type: ActionTypes.EMAIL_CHANGED, payload: string }
export interface PasswordChangedAction { type: ActionTypes.PASSWORD_CHANGED, payload: string}

interface LoginUserSuccess { type: ActionTypes.LOGIN_USER_SUCCESS, payload:any}
interface LoginUserError { type: ActionTypes.LOGIN_USER_ERROR, payload:any}
interface DoingLogin {type: ActionTypes.DOING_LOGIN};

export function emailChanged(email: string): EmailChangedAction {
    return {
        type:  ActionTypes.EMAIL_CHANGED,
        payload: email
    };
};

export function passwordChanged(password: string): PasswordChangedAction {
    return {
        type:  ActionTypes.PASSWORD_CHANGED,
        payload: password
    };
};

function loginUserSuccess(dispatch: any, user: any) {
    dispatch( {
        type: ActionTypes.LOGIN_USER_SUCCESS,
        payload: user
    });

   // Actions.main();
}

function loginUserError(dispatch: any, error: any) {
    dispatch( {
        type: ActionTypes.LOGIN_USER_ERROR,
        payload: error
    })
}

function doingLogin(dispatch: any){
    dispatch( {
        type: ActionTypes.DOING_LOGIN
    })
}

export function requestLogin(email: string, password: string) {
    console.log('email: ' + email +  ' ; ' + 'password: ' + password);
    return async (dispatch: any) => {
        doingLogin(dispatch);
        try {
            const user = await AuthService.credentialsLogin(email, password);
            loginUserSuccess(dispatch, user);
        }catch(e) {
            if (e instanceof FirebaseAuthError) {
                loginUserError(dispatch,e);
            }
        }
    }
};

export function requestGoogleLogin() {
    return async (dispatch: any) => {
        doingLogin(dispatch);
        try {
            const user = await AuthService.googleLogin();
            loginUserSuccess(dispatch, user);
        }catch(e) {
            if (e instanceof FirebaseAuthError){
                loginUserError(dispatch, e);
            }
        }
    }
}

export function requestFacebookLogin() {
    return async (dispatch: any) => {
        doingLogin(dispatch);
        try {
            const user = await AuthService.facebookLogin();
            loginUserSuccess(dispatch, user);
        }catch(e) {
            if (e instanceof FirebaseAuthError){
                loginUserError(dispatch, e);
            }
        }
    }
}

export type Action = EmailChangedAction | PasswordChangedAction | LoginUserSuccess | LoginUserError | DoingLogin;