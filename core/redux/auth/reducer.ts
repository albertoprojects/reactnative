
import { Action, ActionTypes} from './actions';

export interface State {
    email: string,
    password: string,
    user: any,
    error: string,
    loading: boolean
}

export const initialState: State = {
    email: 'demo@demo.com',
    password: 'ddemoo',
    user: null,
    error: '',
    loading: false
}

export function reducer(state = initialState, action: Action): State {
    console.log(action);
    switch (action.type) {
        case ActionTypes.DOING_LOGIN:
            return {...state, loading: true}
        case ActionTypes.EMAIL_CHANGED: 
            console.log(action);
            return { ...state, email: action.payload}
        case ActionTypes.PASSWORD_CHANGED: 
            console.log(action);
            return { ...state, password: action.payload}
        case ActionTypes.LOGIN_USER_SUCCESS:
            return {...state, ...initialState, user: action.payload, loading: false}
        case ActionTypes.LOGIN_USER_ERROR:
            return {...state, error: 'Login user error', loading: false}
        default:
            return state;
    }
}