
import { Action, ActionTypes} from './actions';
import LocalizedStrings from 'react-native-localization';


let strings = new LocalizedStrings({
    "en": {
        SINGUP_USER_ERROR: "Error doing signup"
    },
    "es": {
        SINGUP_USER_ERROR: "Error al registrarse"
    }
});

export interface State {
    error: string,
    loading: boolean,
    user: any
}

export const initialState: State = {
    error: '',
    loading: false,
    user: null
}

export function reducer(state = initialState, action: Action): State {
    console.log(action);
    switch (action.type) {
        case ActionTypes.DOING_SIGNUP:
            return {...state, loading: true}
        case ActionTypes.SIGNUP_USER_SUCCESS:
            return {...state, ...initialState, user: action.payload, loading: false}
        case ActionTypes.SIGNUP_USER_ERROR:
            return {...state, error: strings.SINGUP_USER_ERROR, loading: false}
        default:
            return state;
    }
}