//import firebase from 'firebase';
//import { Actions } from 'react-native-router-flux';
import AuthService from '../../services/firebase/auth/authService';

export enum ActionTypes {
    SIGNUP_USER_SUCCESS = '[SIGNUP] SIGNUP_USER_SUCCESS',
    SIGNUP_USER_ERROR = '[SIGNUP] SIGNUP__USER_ERROR',
    DOING_SIGNUP = '[SIGNUP] DOING_SIGNUP'
}


interface SignupUserError { type: ActionTypes.SIGNUP_USER_ERROR, payload: any}
interface SignupUserSuccess { type: ActionTypes.SIGNUP_USER_SUCCESS, payload: any}
interface DoingSignup { type: ActionTypes.DOING_SIGNUP, payload: any}


function signupUserSuccess(dispatch: any, user: any) {
    dispatch({
        type: ActionTypes.SIGNUP_USER_SUCCESS,
        payload: user
    })
}

function signupUserError(dispatch: any, error: any) {
    dispatch({
        type: ActionTypes.SIGNUP_USER_ERROR,
        payload: error
    })
}

function doingSignup(dispatch: any ){
    dispatch({
        type: ActionTypes.DOING_SIGNUP
    });
}

export function requestSignUp(email: string, password: string, name: string) {
    return (dispatch: any) => {
        doingSignup(dispatch);
        AuthService.emailSignUp(email, password, name) .then( (user: any) => {
            console.log(user);
            signupUserSuccess(dispatch,user);
        }).catch((error: any) => {
            signupUserError(dispatch, error);
        });

    }
}

export type Action = SignupUserSuccess  | SignupUserError | DoingSignup;