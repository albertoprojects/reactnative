import * as auth from '../../translations/auth.json';
import * as signup from '../../translations/signup.json';
import { loadTranslations, setLocale, syncTranslationWithStore } from 'react-redux-i18n';


export default class Translation {

    private  static _instance: Translation = new Translation();
    
    private translationsList = [
        {
            name: "auth",
            file: auth
        },
        {
            name: "signup",
            file: signup
        }
    ]
    private languages = [
        'en',
        'es'
    ]
    private translations: any= {}

    private constructor() {
        this.languages.forEach(language => {
            this.translations[language] = {};
            this.translationsList.forEach((translationElement: {name: string, file:any}) => {
                this.translations[language][translationElement.name] = translationElement.file[language];
            });
        });
        console.log(this.translations);
    }

    public static initTranslation(store: any) {
        syncTranslationWithStore(store);
        return loadTranslations(Translation._instance.translations);
    }

    public static setLocale(lang: string) {
        return setLocale(lang);
    }
}

