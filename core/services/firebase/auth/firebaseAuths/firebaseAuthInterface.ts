import firebase, { AuthCredential, RNFirebase } from "react-native-firebase";

export class FirebaseAuthError extends Error {
 
}

export interface EmailPassCredentials   {
    email: string;
    password: string;
}

export abstract class FirebaseAuth {
    public abstract async login(loginInfo?: any):Promise<Object>
    protected abstract signInAndRetrieveDataWithCredentials(credentials: AuthCredential | EmailPassCredentials): Promise<RNFirebase.UserCredential>
    protected getUserObject(firebaseUser: RNFirebase.UserCredential) {
        return firebaseUser.user.toJSON();
    }
} 