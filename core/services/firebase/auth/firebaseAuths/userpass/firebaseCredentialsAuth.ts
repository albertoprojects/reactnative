
import firebase  from 'react-native-firebase';
import { EmailPassCredentials, FirebaseAuth } from '../firebaseAuthInterface';


export default class FirebaseCredentialsAuth extends FirebaseAuth {
    public async login(loginInfo: EmailPassCredentials) {
        try{
            const currentUser = await this.signInAndRetrieveDataWithCredentials(loginInfo);
            return this.getUserObject(currentUser);
        }catch(error){
            throw(error);
        } 
    }
    protected signInAndRetrieveDataWithCredentials(loginInfo: EmailPassCredentials ) {
       return firebase.auth().signInWithEmailAndPassword(loginInfo.email, loginInfo.password);
    }
}
