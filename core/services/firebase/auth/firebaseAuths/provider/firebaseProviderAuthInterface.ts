import { AuthCredential, RNFirebase } from "react-native-firebase";
import firebase  from 'react-native-firebase';
import { FirebaseAuth, FirebaseAuthError } from '../firebaseAuthInterface';

export default abstract class FirebaseProviderAuth extends FirebaseAuth {
    public async  login() {
        try{
            const data = await this.doLogin();
            // create a new firebase credential with the token
            const providerCredentials = this.getCredential(data);
            // login with credential
            const currentUser = await this.signInAndRetrieveDataWithCredentials(providerCredentials);
    
            return this.getUserObject(currentUser);
        }catch(error) {
            throw new FirebaseAuthError('Something unknown went wrong: '+ error);
        }
    };
    protected signInAndRetrieveDataWithCredentials(credential: AuthCredential): Promise<RNFirebase.UserCredential> {
        return firebase.auth().signInWithCredential(credential);
    }
    protected abstract getCredential(data: any): AuthCredential;
    protected abstract doLogin(): Promise<any>;
}