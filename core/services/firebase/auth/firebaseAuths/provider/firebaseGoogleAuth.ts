import FirebaseProviderAuth from "./firebaseProviderAuthInterface";
import firebase  from 'react-native-firebase';
import { AuthCredential } from "react-native-firebase";
import GoogleAuth from './reactAuths/googleAuth';

export default class FirebaseGoogleAuth extends FirebaseProviderAuth {
    doLogin() {
       return GoogleAuth.login();
    }
    getCredential(data: any): AuthCredential {
        return firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
    }
    
}