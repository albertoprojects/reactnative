import Auth, {UserCancelError, ObtainTokenError, UnknownAuthError} from "./authInterface";
import { GoogleSignin, StatusCodes } from 'react-native-google-signin';

export class GoogleAutExceptions {
    
}

export default class GoogleAuth extends Auth {

    static async login() {
        try {
            // Add any configuration settings here:
            await GoogleSignin.configure();
            const data = await GoogleSignin.signIn();
            return data;
          } catch (error) {
            throw new UnknownAuthError('Something unknown went wrong: ' + error);
          }
    }
}