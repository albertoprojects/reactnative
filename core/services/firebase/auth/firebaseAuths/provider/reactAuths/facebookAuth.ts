import Auth, {UserCancelError, ObtainTokenError, UnknownAuthError} from "./authInterface";
import { LoginManager, AccessToken } from 'react-native-fbsdk';

export default class FacebokAuth implements Auth {
    static async login() {
        try {
            const result : any = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        
            
            if (result.isCancelled) {
                throw new UserCancelError('User cancelled request'); // Handle this however fits the flow of your app
            }

            if (result.error) {
                throw new UnknownAuthError('Something unknown went wrong: ' + result.error);
            }
        
            console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
        
            // get the access token
            const data = await AccessToken.getCurrentAccessToken();
            if (!data) {
                throw new ObtainTokenError('Something went wrong obtaining the users access token'); // Handle this however fits the flow of your app
            }
            return data;
        } catch (error) {
            throw new UnknownAuthError('Something unknown went wrong: ' + error);
        }
    }
}