export default class Auth {
    public static login: (credentials: any) => Promise<any>
}

export class UserCancelError extends Error {

}
export class ObtainTokenError extends Error {

}
export class UnknownAuthError extends Error {

}
export class CredentialsError extends Error {
    
}
