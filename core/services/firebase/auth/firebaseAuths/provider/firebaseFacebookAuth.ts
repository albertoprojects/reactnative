import FirebaseProviderAuth from "./firebaseProviderAuthInterface";
import firebase  from 'react-native-firebase';
import { AuthCredential } from "react-native-firebase";
import FacebokAuth from "./reactAuths/facebookAuth";

export default class FirebaseFacebokAuth extends FirebaseProviderAuth {
    doLogin()  {
        return FacebokAuth.login();
    }
    getCredential(data: any) : AuthCredential{
        return firebase.auth.FacebookAuthProvider.credential(data.accessToken);
    }
}