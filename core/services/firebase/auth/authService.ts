import { Observable, Observer } from 'rxjs';
import { DocumentReference } from 'react-native-firebase/firestore';
import FirebaseFacebookAuth from './firebaseAuths/provider/firebaseFacebookAuth';
import FirebaseGoogleAuth from './firebaseAuths/provider/firebaseGoogleAuth';
import FirebaseCredentialsAuth from './firebaseAuths/userpass/firebaseCredentialsAuth';
import { FirebaseAuth, EmailPassCredentials } from './firebaseAuths/firebaseAuthInterface';
import { Functions } from '../functions/functions';
import firebase, {App} from 'react-native-firebase';


interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  country?: string;
}


export default class AuthService {
  private static _instance: AuthService;

  private user: Observable<User>;
  private userRef: DocumentReference;
  private firebaseApp: App;

  public static init(firebaseApp: App) {
    if (!AuthService._instance) {
      AuthService._instance = new AuthService(firebaseApp);
    }
  }

  private constructor(firebaseApp: App) {
    this.firebaseApp = firebaseApp;
    this.user = new Observable((observer: Observer<User>) => {
      
      this.firebaseApp.auth().onAuthStateChanged((user: any) => {
        this.authChange(user, observer, 'authstatechanged');
      });
      this.firebaseApp.auth().onIdTokenChanged((user: any ) => {
        this.authChange(user, observer, 'tokenchanged');
      });
    });
  }

  private authChange(user: any, observer: Observer<User>, from: string) {
    if (user) {
      console.log(from);
      console.log(user);
      this.userRef = this.firebaseApp.firestore().doc(`users/${user.uid}`);
      return this.userRef.onSnapshot((user: any) => {
        console.log(from + ' onsnpashot: next');
        console.log(user);
        observer.next(user);
      },() => {
        console.log(from + ' onsnapshot: complete');
        observer.complete();
      });
    }else {
      console.log(from + ' complete')
      observer.complete();
    }
  }

  public static getUserLogged() {
    return AuthService._instance.firebaseApp.auth().currentUser();
  }

  private  static async  updateUserData(user: User): Promise<any> {
    // Sets user data to firestore on login
    AuthService._instance.userRef = await AuthService._instance.firebaseApp.firestore().doc(`users/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    }

    //localStorage.setItem('userData', JSON.stringify(data));

    await AuthService._instance.userRef.set(data, { merge: true });
    return data;
  }

  public static async createUser(user: User): Promise<any> {
     

  }

  static async signOut() {
    return await firebase.auth().signOut();
  }
  static async credentialsLogin(email: string, password: string) {
    let loginInfo: EmailPassCredentials = { email: email, password: password};
    return await AuthService.login(AuthService.getAuth(FirebaseCredentialsAuth), loginInfo);
  }
  
  static async googleLogin() {
    return await AuthService.login(AuthService.getAuth(FirebaseGoogleAuth));
  } 

  static async facebookLogin() {
    return await AuthService.login(AuthService.getAuth(FirebaseFacebookAuth));
  }

  private  static async login(auth: FirebaseAuth, loginInfo?: EmailPassCredentials): Promise<any> {
    try {
      const currentUser = await auth.login(loginInfo) as User;
      return await AuthService.updateUserData(currentUser);
    } catch (error) {
      throw error;
    }
  }

  private static getAuth<T extends FirebaseAuth>(a: new () => T): T {
    return new a();
  }

  static async emailSignUp(email: string, password: string, name: string): Promise<any> {
    try {
      await Functions.checkUsername(name);
      await AuthService._instance.firebaseApp.auth().createUserWithEmailAndPassword(email, password);
      const currentUser = AuthService._instance.firebaseApp.auth().currentUser;
      if (currentUser) {
        const user: User = {
          uid: currentUser.uid,
          email: email,
          displayName: name
        };
        return AuthService.updateUserData(user);
      }
      throw {};
    }catch (error) {
      throw error;
    }
  }
}