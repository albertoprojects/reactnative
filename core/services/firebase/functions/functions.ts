import {App} from 'react-native-firebase';

export class Functions {
    
    private static _instance: Functions;

    private static firebaseApp: App;

    public static init(firebaseApp: App) {
        if (!Functions._instance) {
            Functions._instance = new Functions(firebaseApp);
        }
      }

    private constructor(firebaseApp: App) {
        Functions.firebaseApp = firebaseApp;
    }

    private static callFunction(name: string, params: any) {
        return Functions.firebaseApp.functions().httpsCallable(name)(params)
    }

    public static checkUsername(username: string) {
        return Functions.callFunction('checkUsername', {username: username});
    }
}