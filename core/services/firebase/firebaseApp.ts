import firebase, { App } from 'react-native-firebase';
import Notifications from './messaging/notifications';
import Messages from './messaging/messages';
import AuthService from './auth/authService';
export {App} from 'react-native-firebase';
import { Functions } from './functions/functions';
import { userChanged } from './redux/auth';


export class FirebaseApp  {
    private static config = {
        clientId: "",
        appId: "",
        apiKey: "AIzaSyDMXq8tUDK7nqkxnHpPfqFFpGfwesG8Vfs",
        databaseURL: "",
        storageBucket: "",
        messagingSenderId: "",
        projectId: "",
        persistence: true
      };

    private static app: App;

    public static getApp(): App {
        if (!FirebaseApp.app) {
            FirebaseApp.initApp();
        }
        return FirebaseApp.app;
    }

    private static async initApp() {
        FirebaseApp.app = firebase.app();
        //FirebaseApp.app = firebase.app(FirebaseApp.config, 'PlayLink');
        Notifications.init(FirebaseApp.app);
        Messages.init(FirebaseApp.app);
        Functions.init(FirebaseApp.app);
        AuthService.init(FirebaseApp.app);
        /*
        await AuthService.signOut();

        AuthService.getUser().subscribe((user) => {
            console.log('user');
            console.log(user);
            userChanged(user);
        });*/
    }

    private static async checkLogged() {
        
    }


  }
