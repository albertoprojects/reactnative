import { dispatch } from "rxjs/internal/observable/pairs";

export enum ActionTypes {
    USER_CHANGED = '[FIREBASE] USER_CHANGED'
}

export interface FirebaseUserAction { type: ActionTypes.USER_CHANGED, payload: any }


export function userChanged(user: any) {
    return (dispatch: any) => {
        dispatchUserChanged(dispatch, user);
    }
};

function dispatchUserChanged(dispatch: any, user: any) {
    dispatch( {
        type: ActionTypes.USER_CHANGED,
        payload: user
    });
}


export type Action = FirebaseUserAction;