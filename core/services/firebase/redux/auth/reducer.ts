
import { Action, ActionTypes} from './actions';

export interface State {
    user: any
}

export const initialState: State = {
    user: null
}

export function reducer(state = initialState, action: Action): State {
    console.log(action);
    switch (action.type) {
        case ActionTypes.USER_CHANGED:
            return {...state, user: action.payload}
        default:
            return state;
    }
}