import { combineReducers} from 'redux';
import * as auth from './auth';

const reducers = {
    firebaseAuth: auth.reducer
}

export default reducers;