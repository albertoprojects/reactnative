import firebase, { RNFirebase } from 'react-native-firebase';
import { Observable, Subscriber } from 'rxjs';
import {App} from 'react-native-firebase';

export default class Notifications {
    private static _instance: Notifications;

    private firebaseApp: App;

    public onNotification!: Observable<RNFirebase.notifications.Notification>;
    public onNotificationDisplayed!:Observable<RNFirebase.notifications.Notification>;
    public onNotificationOpened!: Observable<RNFirebase.notifications.NotificationOpen>;

    private onNotificationListener!: () => any;
    private onNotificationDisplayedListener!: () => any;
    private onNotificationOpenedListener!: () => any;

    public static init(firebaseApp: App) {
        if (Notifications._instance == null) {
            Notifications._instance = new Notifications(firebaseApp);
        }
    }

    private constructor(firebaseApp: App) {
        this.firebaseApp = firebaseApp;
        this.createChannel();
        this.listenNotifications();
        console.log('constructor');
    } 

    private createChannel() {
        const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('My apps test channel');
    
        // Create the channel
        this.firebaseApp.notifications().android.createChannel(channel);
    }

    private listenNotifications() {
        this.onNotification = new Observable((subscriber) => {
            this.onNotificationListener = this.firebaseApp.notifications().onNotification( (notification) => {
                console.log('onNotification');
                subscriber.next(notification);
            });
        });

        this.onNotificationDisplayed = new Observable((subscriber) => {
            this.onNotificationDisplayedListener = this.firebaseApp.notifications().onNotificationDisplayed( (notification) => {
                console.log('onNotificationDisplayed');
                subscriber.next(notification);
            });
        });

        this.onNotificationOpened = new Observable((subscriber) => {
            this.onNotificationOpenedListener = this.firebaseApp.notifications().onNotificationOpened( (notification) => {
                console.log('onNotificationOpened');
                subscriber.next(notification);
            });
        });
    }  
}