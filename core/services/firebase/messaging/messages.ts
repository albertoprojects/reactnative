import firebase  from 'react-native-firebase';
import {App} from 'react-native-firebase';
import { Observable } from 'rxjs';

export default class Messages {
    private static _instance: Messages;

    private firebaseApp: App;

    public onMessage!: Observable<any>;

    public onBgMessageObsv!: Observable<any>;

    public static init(firebaseApp: App) {
        if (!Messages._instance) {
            Messages._instance = new Messages(firebaseApp);
        }
    }

    private constructor(firebaseApp: App) {
        this.firebaseApp = firebaseApp;
        this.initMessages();
    } 

    private async initMessages() {
        if (await this.chekPermission()){
            this.listenMessages();
        }
    }

    private async chekPermission() {
        let enabled = await firebase.messaging().hasPermission();
        if (!enabled) {
            enabled = await this.requestPermission();
        }
        return enabled;
    }

    private async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            return true;
        } catch (error) {
            return false;
        }
    }

    private listenMessages() {
        //Data only messages
        this.onMessage = new Observable((subscriber) => {
            this.firebaseApp.messaging().onMessage( (message) => {
                console.log('onMessage');
                subscriber.next(message);
            });
        });
    }
}

 //When app closed
 export async function onBgMessage(message: any) {
    //firebase.notifications().displayNotification(message);
    const notification = new firebase.notifications.Notification()
    .setNotificationId('notificationId')
    .setTitle('My notification title')
    .setBody('My notification body')
    .setData({
        key1: 'value1',
        key2: 'value2',
    });
    return firebase.notifications().displayNotification(notification);
}