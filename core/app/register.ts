/** @format */
  
import {AppRegistry} from 'react-native';
import {onBgMessage} from '../services/firebase/messaging/messages';

export default class Register {
    public static register(App: any, appName: string) {
        AppRegistry.registerComponent(appName, () => App);
        //AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => onBgMessage);
    }
}
